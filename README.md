# Node + Express Tutorial


## Info
- A simple Node and Express project collecting emails (leads) with CRUD functionality.
- Add jquery ajax delete lead
- Add bootstrap for nicer frontend
- [Repo](https://gitlab.com/miltozz/node-express-learn)
- Project without ajax+bootstrap: [here](https://gitlab.com/miltozz/node-express-basics)

## Run
Main branch is configured for use in localhost

- `config/database.json`
- Needs installed postgres with `express-mvp-dbuser` and `express-mvp-db` on 127.0.0.1
- Needs node and npm locally
- jquery and bootstrap load from CDNs

## psql

- postgres=# `create user "express-mvp-dbuser" with encrypted password '123456';`
- postgres=# `create database "express-mvp-db" with owner = "express-mvp-dbuser";`

## Uses
- [Postgres](https://www.postgresql.org/download/linux/ubuntu/)
    - **In Gitpod use postgres with** `image: gitpod/workspace-postgres` in `.gitpod.yml`
- [Sequelize ORM](https://sequelize.org/)
    - sequelize init applies initial conf and creates `config/database.json` and required folders
    - apply migrations with `sequelize db:migrate`
- `npm install --save sequelize`
- `npm install --save pg pg-hstore`
- `npm install --save-dev sequelize-cli`

## Links
- [Tutorial Video](https://www.youtube.com/watch?v=G8uL0lFFoN0)
- [Tutorial Repo](https://github.com/buzz-software/expressjs-mvp-landing-page)

Gitlab NodeExpress Template used as base, not Tutorial Repo.

## Gitpod
Start Gitpod default branch:
`gitpod.io/#https://github.com/gitpod-io/website`

Start Gitpod Workspace with specific branch:
 `gitpod.io/#https://github.com/gitpod-io/website/tree/my-branch`

